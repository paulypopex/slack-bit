const tools = require('../src/tools')
const request = require('request')
let callback = null

describe('tools', () => {
  const config = {
    tmpFolder: '/tmp'
  }
  let activity = null

  beforeEach(function () {
    sandbox.stub(console, 'info')
    sandbox.stub(console, 'warn')
    sandbox.stub(tools, 'getConfig').returns(config)
    sandbox.stub(request, 'post').yields()
    callback = sandbox.stub()
    activity = {
      duration: 666,
      distance: 6.666,
      distanceUnit: 'kilometres',
      startTime: '2018-09-20T06:07:08'
    }
  })

  afterEach(function () {
    sandbox.restore()
  })

  describe('unit', () => {
    it('makes a short unit from activity', () => {
      expect(tools.unit(activity)).to.equal('k')
    })
  })

  describe('distance', () => {
    it('makes distance for display', () => {
      expect(tools.distance(activity)).to.equal('6.7k')
    })
  })

  describe('time', () => {
    it('makes time of date for display', () => {
      expect(tools.time(activity)).to.equal('06:07')
    })
  })

  describe('duration', () => {
    it('makes duration for display, in minutes and seconds', () => {
      expect(tools.duration(activity.duration)).to.equal("11'06")
    })
  })

  describe('updateStatus', () => {
    beforeEach(() => {
      tools.updateStatus('foo', ':bar:', callback)
    })

    it('makes duration for display, in minutes and seconds', () => {
      expect(request.post).to.have.been.called()
    })
  })
})
