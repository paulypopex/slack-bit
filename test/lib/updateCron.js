const childProcess = require('child_process')
const fs = require('fs')
const updateCron = require('../../src/lib/updateCron')

describe('updateCron', () => {
  let callback = null

  beforeEach(() => {
    callback = sandbox.stub()
    sandbox.stub(childProcess, 'exec').yields(null, '* * * * * foo.js\n* * * * * bar.js')
    sandbox.stub(fs, 'writeFile').yields(null)
  })

  afterEach(() => sandbox.restore())

  describe('with new matching entries', () => {
    beforeEach(() => {
      updateCron('etc.js', '* * * * *', callback)
    })

    it('is ok', () => {
      expect(callback).to.have.been.calledOnce()
        .and.calledWithExactly(null)
    })
  })

  describe('with previously matching matching entries', () => {
    beforeEach(() => {
      updateCron('foo.js', '* * * * *', callback)
    })

    it('does not overwrite them', () => {
      expect(callback).not.to.have.been.called()
    })
  })

  describe('when we cannot update the crontab', () => {
    beforeEach(() => {
      childProcess.exec.onCall(1).yields('oops')
      updateCron('etc.js', '* * * * *', callback)
    })

    it('yields the error', () => {
      // const expected = sandbox.match.instanceOf(Error).that.has.property('message').that.matches(/oops/)
      // .and(sandbox.match.has('message', 'Could not get provider from params'))
      expect(callback).to.have.been.calledOnce()
        .and.calledWithExactly(sandbox.match.instanceOf(Error))
    })
  })

  describe('when we cannot write the temp file', () => {
    beforeEach(() => {
      fs.writeFile.yields('oops')
      updateCron('etc.js', '* * * * *', callback)
    })

    it('yields the error', () => {
      expect(callback).to.have.been.calledOnce()
        .and.calledWithExactly(sandbox.match.instanceOf(Error))
    })
  })
})
