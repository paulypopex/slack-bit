#!/usr/bin/env node

// ad hoc slack status

const { getConfig, updateStatus } = require('./tools')
const { writeFile } = require('fs')
const { name } = require('../package')
const { tempFolder = '/tmp' } = getConfig()

const statusFile = `${tempFolder}/${name}-status.txt`

const args = process.argv.slice(2)
const icon = `:${args.shift()}:`
const status = args.join(' ')
console.log({ icon, status })

updateStatus(status, icon, err => {
  if (err) throw err
  writeFile(statusFile, status, err => {
    if (err) console.error(err)
  })
})
