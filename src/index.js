#!/usr/bin/env node

// update slack status with my latest run stats

const { getLastRun, buildAndUpdateProfileImage, clock, distance, getConfig, icon, pace, updateStatus } = require('./tools')
const { updateCron } = require('./lib')
const { name } = require('../package')
const { readFile, writeFile } = require('fs')
const { tempFolder = '/tmp' } = getConfig()

const date = process.argv[2] ? new Date(process.argv[2]) : new Date()
const dateStamp = date.toISOString().substr(0, 10)

getLastRun(dateStamp, (err, run) => {
  if (err) throw err
  if (!run) return console.log('no run yet')
  const status = `${clock(run)} ${distance(run)}, ${pace(run)}`
  const statusFile = `${tempFolder}/${name}-status.txt`
  readFile(statusFile, (ignoredErr, content) => {
    console.log({ status, content, run })
    if ('' + content === status) return console.log('status still', status, 'so not updating anything')
    updateStatus(status, icon(run), err => {
      if (err) throw err
      buildAndUpdateProfileImage(run, err => {
        if (err) throw err
        writeFile(statusFile, status, ignoredErr => {
        })
      })
    })
  })
})

updateCron(process.argv[1], '*' + '/10 7-13 * * 1-5', err => {
  if (err) console.error(err)
})
