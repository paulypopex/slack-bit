#!/usr/bin/env node

const express = require('express')
const { getConfig, getFitbitToken, setConfig } = require('./tools')
const config = getConfig()
const scope = (config.fitbit && config.fitbit.scope) || 'activity weight social'
const client_id = process.env.FITBIT_CLIENT_ID || (config.fitbit && config.fitbit.client_id)
const client_secret = process.env.FITBIT_CLIENT_SECRET || (config.fitbit && config.fitbit.client_secret)

// write a fitbit auth token to the config file

const app = express()

app.get('/', function (req, res) {
  const host = req.headers.host
  if (!req.query.code) return res.redirect(`https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=${client_id}&redirect_uri=http%3A%2F%2F${host}&scope=${scope}`)
  getFitbitToken(host, req.query.code, (err, json) => {
    if (err || !json) return res.send(err || 'oops')
    config.fitbit = Object.assign({}, config.fitbit, json)
    config.fitbit.client_id = client_id
    config.fitbit.client_secret = client_secret
    setConfig(config, err => {
      res.send(err || '👍 Written your access token, ok to close this window')
      process.exit(err ? 1 : 0)
    })
  })
})

app.listen(5678)
