const { name } = require('../../package')
const childProcess = require('child_process')
const fs = require('fs')
const tempFolder = '/tmp'

module.exports = (file, frequency, callback) => {
  childProcess.exec(`crontab -l`, (ignoreError, crontab) => {
    crontab = ('' + crontab).split('\n')
    const regex = new RegExp(file.replace(/.\w+$/, '').replace(/([^a-zA-Z0-9])/g, '\\$1'))
    const tempFileBase = `${tempFolder}/${name}-${file.split('/').pop().replace(/\W+/g, '-')}`
    if (crontab.find(line => regex.test(line))) return // already got it
    crontab.push(`# recommended for ${file}, added automatically ${new Date()}, edit at will:`)
    crontab.push(`${frequency} bash -l -c '${file}' > ${tempFileBase}.out 2>&1`)
    const tempFile = `${tempFileBase}.tmp`
    fs.writeFile(tempFile, crontab.join('\n') + '\n', err => {
      if (err) return callback(new Error(`🙀 failed to write temporary file ${tempFile} (${err}), please see a friendly engineer`))
      childProcess.exec(`crontab ${tempFile}`, err => {
        if (err) return callback(new Error(`🙀 failed to set new cron (${err}), please see a friendly engineer`))
        callback(null)
      })
    })
  })
}
