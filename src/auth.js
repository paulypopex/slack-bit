#!/usr/bin/env node

const express = require('express')
const { getConfig, getSlackToken, setConfig } = require('./tools')
const config = getConfig()
const scope = 'users.profile:read,users.profile:write,emoji:read,reactions:read,reactions:write,search:read,chat:write:bot,files:read,files:write:user'
const client_id = process.env.SLACK_CLIENT_ID || (config.slack && config.slack.client_id)

// write a slack auth token to the config file

const app = express()

app.get('/', function (req, res) {
  const host = req.headers.host
  if (!req.query.code) return res.redirect(`https://slack.com/oauth/authorize?scope=${scope}&client_id=${client_id}&redirect_uri=http%3A%2F%2F${host}`)
  getSlackToken(host, req.query.code, (err, json) => {
    console.log({ err, json })
    if (err || !json) return res.send(err || 'oops')
    config.slack = Object.assign({}, config.slack, json)
    setConfig(config, err => {
      res.send(err || '👍 Written your access token, ok to close this window')
      process.exit(err ? 1 : 0)
    })
  })
})

app.listen(5678)
