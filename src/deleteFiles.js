#!/usr/bin/env node

const { deleteFile, listFilesOlderInDaysThan, postMessage } = require('./tools')
const { updateCron } = require('./lib')

const debug = process.argv[2]
const daysToGoBack = 7
const filesToDealWith = debug ? 3 : 100
const username = 'delete-o-matic'
const icon_emoji = ':put_litter_in_its_place:' // eslint-disable-line camelcase

listFilesOlderInDaysThan(daysToGoBack, (ignoredError, filesToDelete = []) => {
  listFilesOlderInDaysThan(daysToGoBack - 1, (ignoredError, filesToWarnAbout = []) => {
    let channel = 'C0NDQHP0R' // general
    // if (debug)
    channel = 'C6Q35H17F' // fitbit test channel
    console.log(filesToDelete.map(file => `${file.name} ${(file.size / 1024 / 1024).toFixed(1)}MB ${new Date(file.timestamp * 1000).toDateString()}`))
    filesToWarnAbout = filesToWarnAbout.slice(0, filesToDealWith)
    filesToDelete = filesToDelete.slice(0, filesToDealWith)
    if (!filesToWarnAbout.length && !filesToDelete.length) return
    const text = `Deleting ${filesToDelete.length} today, ${filesToWarnAbout.length} next time > ${daysToGoBack} days old`
    postMessage({ text, channel, username, icon_emoji }, (err, response) => {
      if (err) return console.warn({ err, text })

      filesToWarnAbout.forEach(file => {
        // console.log(file.name, file.size)
        if (!file.thumb_360) return
        const text = `Deleting next time: ${(file.size / 1024 / 1024).toFixed(1)}MB ${file.title} ${new Date(file.timestamp * 1000).toDateString()}` // ${file.thumb_64}`
        const blocks = [
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text
            }
          }
        ]
        if (file.thumb_360) {
          blocks[0].accessory = {
            type: 'image',
            image_url: file.thumb_360,
            alt_text: file.title
          }
        }
        const thread_ts = response.ts // eslint-disable-line camelcase
        postMessage({ text, channel, thread_ts, username, blocks }, (err) => {
          if (err) return console.error(err, blocks)
        })
      })

      if (debug) return

      filesToDelete.forEach(file => {
        deleteFile(file.id, (err) => {
          if (err) return console.error(err)
          const text = `Deleted ${file.title} ${new Date(file.timestamp * 1000).toDateString()}`
          const thread_ts = response.ts // eslint-disable-line camelcase
          postMessage({ text, channel, thread_ts, username }, (err) => {
            if (err) return console.error(err)
          })
        })
      })
    })
  })
})

updateCron(process.argv[1], '0 12 * * *', err => {
  if (err) console.error(err)
})
