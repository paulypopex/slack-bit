/* eslint-disable camelcase */

const { createReadStream, writeFile, readdir, readdirSync, readFile } = require('fs')
const request = require('request')
const childProcess = require('child_process')
const { promisify } = require('util')
const { dirname, resolve } = require('path')
let jimp = null
try {
  jimp = require('jimp')
} catch (e) {
  // console.warn(e, 'but jimp is optional')
}
const btoa = require('btoa')
const { GarminConnect } = require('garmin-connect')
const GCClient = new GarminConnect()

const tools = module.exports = {}

const json = true
const offset = 0
const limit = 10

const configFile = tools.configFile = resolve(process.env.HOME, 'slack-bit.json')

const getConfig = tools.getConfig = () => {
  return getOptionalJSONFile(configFile)
}

const getOptionalJSONFile = tools.getOptionalJSONFile = file => {
  try {
    return require(file)
  } catch (e) {
    return {}
  }
}

const { fitbit = {}, slack = {} } = getConfig()

tools.setConfig = (config, callback) => {
  return setJSONFile(configFile, config, callback)
}

const reorder = unordered => {
  if (Array.isArray(unordered)) {
    return unordered.map(reorder)
  }
  if (!Object.keys(unordered)) {
    return unordered
  }
  return Object.keys(unordered).sort().reduce((obj, key) => {
    obj[key] = unordered[key]
    return obj
  }, {})
}

tools.reorder = reorder

const setJSONFile = tools.setJSONFile = (file, data, callback) => {
  writeFile(file, JSON.stringify(data, null, 2), err => {
    callback(err ? `🙀 chmod +w ${file} and try again` : null)
  })
}

tools.setConfig = (config, callback) => {
  writeFile(configFile, JSON.stringify(config, null, 2), err => {
    callback(err ? `🙀 chmod +w ${configFile} and try again` : null)
  })
}

const unit = tools.unit = activity => {
  if (!activity.distanceUnit) return ''
  return `${('' + activity.distanceUnit).substr(0, 1)}`
}

const pace = tools.pace = (activity, distancePerStep) => {
  if (!activity.pace) {
    // console.log({ activity })
    // console.log('so pace', (activity.duration / 1000) / actualDistance(activity, distancePerStep))
    // return `pace? ran for ${tools.duration(activity.duration / 1000)}`
    return `${tools.duration((activity.duration / 1000) / actualDistance(activity, distancePerStep))}/${unit(activity)}`
  }
  return `${tools.duration(activity.pace)}/${unit(activity)}`
}

tools.icon = activity => {
  if (/treadmill/i.test(activity.activityName)) return ':hamster:'
  if (/walk/i.test(activity.activityName)) return ':walking:'
  return ':runner:'
}

const actualDistance = (activity, distancePerStep) => {
  if (activity.distance) return activity.distance
  if (activity.steps && distancePerStep) {
    console.log('estimating based on', activity.steps, 'x', distancePerStep)
    return activity.steps * distancePerStep
  }
}

const distance = tools.distance = (activity, distancePerStep) => {
  const distance = actualDistance(activity, distancePerStep)
  if (!distance) return 'distance? (no GPS?)'
  return `${distance.toFixed(1)}${unit(activity)}`
}

const date = tools.date = activity => activity.startTime.substr(0, 10)

const duration = tools.duration = seconds => `${Math.floor(seconds / 60)}'${String(Math.floor(seconds % 60)).padStart(2, '0')}`

tools.nonEmpty = foo => foo

const time = tools.time = activity => {
  const time = new Date(activity.startTime)
  const pad = n => n < 10 ? `0${n}` : n
  const hour = time.getHours()
  let minutes = time.getMinutes()
  return `${pad(hour)}:${pad(minutes)}`
}

tools.clock = activity => {
  const time = new Date(activity.startTime)
  const hour = (time.getHours() % 12) || 12
  let minutes = time.getMinutes()
  const clockEmoji = minutes > 30 ? `:clock${hour}30:` : `:clock${hour}:`
  return clockEmoji
}
tools.getFitbitToken = (host, code, callback) => {
  const url = 'https://api.fitbit.com/oauth2/token'
  const grant_type = 'authorization_code'
  const redirect_uri = `http://${host}`
  const expires_in = 31536000
  const qs = { expires_in, grant_type, redirect_uri, code }
  const client_id = process.env.FITBIT_CLIENT_ID || fitbit.client_id
  const client_secret = process.env.FITBIT_CLIENT_SECRET || fitbit.client_secret
  const auth = btoa(`${client_id}:${client_secret}`)
  const headers = {
    'Authorization': `Basic ${auth}`,
    'Content-Type': 'application/x-www-form-urlencoded'
  }
  request.post({ url, qs, headers, json }, (err, response, body) => {
    // console.log({ client_id, client_secret, url, qs, headers, err, body })
    callback(err, body)
  })
}

tools.updateStatus = (status_text = '', status_emoji = '', callback) => {
  const token = process.env.SLACK_TOKEN || slack.access_token
  const finishTime = new Date()
  finishTime.setHours(17) // @todo config this
  finishTime.setMinutes(30)
  const status_expiration = Math.round(finishTime.getTime() / 1000)
  const profile = JSON.stringify({ status_text, status_emoji, status_expiration })
  const url = 'https://slack.com/api/users.profile.set'
  const qs = { token, profile }
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
  request.post({ url, qs, headers, json }, (err, response, body) => {
    console.log({ url, qs, err, body })
    err = err || (body && body.error)
    if (callback) return callback(err, body)
    if (err) console.warn(err)
  })
}

tools.deleteFile = (file, callback) => {
  const token = process.env.SLACK_TOKEN || slack.access_token
  const url = 'https://slack.com/api/files.delete'
  const qs = { token, file }
  request.post({ url, qs, json }, (err, response, body) => {
    console.log({ url, qs, err, body })
    err = err || (body && body.error)
    if (callback) return callback(err, body)
    if (err) console.warn(err)
  })
}

const byTimestamp = (a, b) => {
  return a.timestamp - b.timestamp
}

tools.listFilesOlderInDaysThan = (days, callback) => {
  const token = process.env.SLACK_TOKEN || slack.access_token
  const url = 'https://slack.com/api/files.list'
  const date = new Date()
  date.setDate(date.getDate() - days)
  const ts_to = Math.round(date.getTime() / 1000) // eslint-disable-line
  const sort = 'timestamp'
  // const types = 'videos,images'
  const show_files_hidden_by_limit = true // eslint-disable-line
  // const user = slack.user_id // eslint-disable-line // not needed because I am now admin
  const qs = { token, sort, ts_to, show_files_hidden_by_limit }
  request.get({ url, qs, json }, (err, response, body) => {
    const debug = (body && body.files && body.files.map(file => file.name + ' ' + file.size)) || body
    console.log({ url, qs, err, debug })
    err = err || (body && body.error)
    const files = ((body && body.files) || []).sort(byTimestamp)
    if (callback) return callback(err, files)
    if (err) console.warn(err)
  })
}

tools.postMessage = ({ text = '', channel = 'general', blocks, thread_ts, username, icon_emoji = ':robot:' }, callback) => {
  const token = process.env.SLACK_TOKEN || slack.access_token
  const as_user = false
  const url = 'https://slack.com/api/chat.postMessage'
  if (Array.isArray(blocks)) blocks = JSON.stringify(blocks)
  const qs = { token, as_user, text, thread_ts, channel, blocks, icon_emoji, username }
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
  request.post({ url, qs, headers, json }, (err, response, body) => {
    // err = err || (body && body.error)
    if (err) console.warn(JSON.stringify({ url, qs, headers, json }, null, 2))
    if (callback) return callback(err, body)
    if (err) console.warn(err)
  })
}

tools.updateProfileImage = (img, callback) => {
  if (!img) return callback(new Error('no image'))
  const token = process.env.SLACK_TOKEN || slack.access_token
  const url = 'https://slack.com/api/users.setPhoto'
  const headers = {
    'Content-Type': 'application/multipart-formdata'
  }
  const formData = {
    token,
    image: createReadStream(img)
  }
  request.post({ url, formData, headers, json }, (err, response, body) => {
    err = err || (body && body.error)
    if (callback) return callback(err, body)
    if (err) console.warn(err)
  })
}

tools.buildProfileImage = (run, callback) => {
  if (!run) return callback(new Error('no run details, so cannot build profile image'))
  if (!jimp) return callback(new Error('no jimp, so cannot build profile image'))
  const imageFolder = resolve(__dirname, 'img')
  const images = readdirSync(imageFolder).filter(img => /^profile.*\.jpg/.test(img))
  if (!images.length) {
    console.warn('no images in', imageFolder)
    return callback(null)
  }
  const originalImage = resolve(imageFolder, images[Math.floor(Math.random() * images.length)])
  const width = 640
  const textBorder = 20
  let leftAsPercentage = 10 // default % actoss image to start writing
  let topAsPercentage = 10 // default % down image to start writing
  const testImageForPositionsInName = /(\d+)x(\d+)/.exec(originalImage)
  if (testImageForPositionsInName) {
    leftAsPercentage = parseInt(testImageForPositionsInName[1], 10)
    topAsPercentage = parseInt(testImageForPositionsInName[2], 10)
  }
  const smallFontSize = 32
  const smallFontCharWidth = Math.floor(smallFontSize / 2) // approx, but fine
  const smallFontLineHeight = Math.floor(smallFontSize * 1.4) // approx, but fine
  let left = Math.floor(leftAsPercentage * (width / 100))
  if (left < textBorder) left = textBorder
  let top = Math.floor(topAsPercentage * (width / 100)) // yeah width, image is basically square
  if (top < textBorder) top = textBorder
  const line = [
    `${date(run)} ${time(run)}`,
    `${distance(run)}, ${duration(run.duration / 1000)}`,
    pace(run)
  ]
  const longest = (longest, line) => line.length > longest ? line.length : longest
  jimp.loadFont(jimp[`FONT_SANS_${smallFontSize}_WHITE`], (err, smallFont) => {
    if (err) throw err
    jimp.read(originalImage, (err, img) => {
      if (err) throw err
      const boxWidth = smallFontCharWidth * line.reduce(longest, 0)
      const boxHeight = smallFontLineHeight * line.length
      const newImage = resolve(__dirname, `img/.profile.jpg`)
      img.resize(width, jimp.AUTO)
      img.resize(width, jimp.AUTO)
        .quality(60)
      for (let i = textBorder; i > 0; i -= 2) {
        const clone = img.clone()
          .blur(2)
          .color([
            { apply: 'darken', params: [1] }
          ])
        img
          .blit(clone, left - i, top - i, left - i, top - i, boxWidth + (i * 2), boxHeight + (i * 2))
      }
      img
        .print(smallFont, left, top, line[0])
        .print(smallFont, left, top + smallFontLineHeight, line[1])
        .print(smallFont, left, top + smallFontLineHeight * 2, line[2])
        .write(newImage, err => {
          callback(err, newImage)
        })
    })
  })
}

tools.buildAndUpdateProfileImage = (run, callback) => {
  tools.buildProfileImage(run, (err, newImage) => {
    if (err) return callback(err)
    if (!newImage) return callback()
    const cmd = `aws s3 cp ${newImage} s3://clarkeology.com/img/profile.jpg`
    childProcess.exec(cmd, (error, stdout, stderr) => {
      if (error) console.error(error)
      if (stderr) console.error(stderr)
      if (stdout) console.log(stdout)
      tools.updateProfileImage(newImage, callback)
    })
  })
}

tools.getSummary = (date, callback) => {
  const url = `https://api.fitbit.com/1/user/-/activities/date/${date}.json`
  const headers = { Authorization: `${fitbit.token_type} ${fitbit.access_token}` }
  request.get({ url, headers, json }, (err, response, body) => {
    err = err || (body && body.errors && body.errors[0])
    if (err) return callback(err)
    if (!body) return callback(new Error('no body'))
    callback(null, body.summary)
  })
}

tools.getHeartData = (date, callback) => {
  const url = `https://api.fitbit.com/1/user/-/activities/heart/date/${date}/1d.json`
  const headers = { Authorization: `${fitbit.token_type} ${fitbit.access_token}` }
  request.get({ url, headers, json }, (err, response, body) => {
    err = err || (body && body.errors && body.errors[0])
    if (err) return callback(err)
    if (!body) return callback(new Error('no body'))
    if (!body['activities-heart']) return callback()
    if (!body['activities-heart'].length) return callback()
    callback(null, body['activities-heart'][0].value)
  })
}

tools.getGarminSummary = (date, callback) => {
  const url = `https://connect.garmin.com/modern/proxy/usersummary-service/usersummary/list/d99659ec-272c-4f3f-8f1d-7cd8753150f7?start=1&limit=10&maxDate=${date}&_=1618261215988`
  const headers = {
    'authority': 'connect.garmin.com',
    'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
    'dnt': '1',
    'sec-ch-ua-mobile': '?0',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36',
    'nk': 'NT',
    'accept': 'application/json, text/javascript, */*; q=0.01',
    'x-requested-with': 'XMLHttpRequest',
    'x-app-ver': '4.41.1.0',
    'x-lang': 'en-US',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'cors',
    'sec-fetch-dest': 'empty',
    'referer': 'https://connect.garmin.com/modern/',
    'accept-language': 'en,en-US;q=0.9',
    'cookie': 'G_ENABLED_IDPS=google; notice_preferences=0:; notice_gdpr_prefs=0:; GARMIN-SSO=1; GarminNoCache=true; GARMIN-SSO-GUID=E7FAC200B441D3EE781DF7981F06DD63443E5CFF; GARMIN-SSO-CUST-GUID=25d79136-6606-42b2-9f6d-fa44e64f6514; __cfduid=da15413d97dad6d99a30d93394ba5b4f61618256907; SESSIONID=4ecb0c09-54ef-4a50-a480-92fc780d3ddb; __cflb=02DiuJLbVZHipNWxN8yYRX3u8XkAfEE59C8H4sUGgsTTS; SameSite=None'
  }

  request.get({ url, headers, json }, (err, response, data) => {
    // console.log({ response, err, data })
    if (err) return callback(err)
    const summary = data.find(line => line.calendarDate === date) || {}
    summary.steps = summary.totalSteps
    callback(err, summary)
  })
}

tools.getWeightLog = (date, callback) => {
  const url = `https://api.fitbit.com/1/user/-/body/log/weight/date/${date}.json`
  const headers = { Authorization: `${fitbit.token_type} ${fitbit.access_token}` }
  request.get({ url, headers, json }, (err, response, body) => {
    err = err || (body && body.errors && body.errors[0])
    if (err) return callback(err)
    if (!body || !body.weight || !body.weight.length) return callback(new Error('no body'))
    callback(null, body.weight[0])
  })
}

tools.isoDate = date => {
  if (!date) return new Date().toISOString().substr(0, 10)
  return new Date(date).toISOString().substr(0, 10)
}

tools.getFitbitActivities = (date, callback) => {
  date = tools.isoDate(date)
  // const url = `https://api.fitbit.com/1/user/-/activities/date/${date}.json`
  const url = `https://api.fitbit.com/1/user/-/activities/list.json`
  const afterDate = date
  const sort = 'asc'
  const qs = { afterDate, offset, limit, sort }
  const headers = { Authorization: `${fitbit.token_type} ${fitbit.access_token}` }
  request.get({ url, headers, qs, json }, (err, response, body) => {
    err = err || (body && body.errors && body.errors[0])
    if (err) return callback(err)
    if (!body) return callback(new Error('no body'))
    if (body.errors) {
      if (body.errors.filter(error => error.errorType === 'invalid_token').length) {
        return callback(new Error('type `npm start` in this folder to open your browser and generate a new token'))
      }
    }
    // seemingly new style auth error
    if (/Authorization Error/.test(body)) {
      return callback(new Error('type `npm start` in this folder to open your browser and generate a new token'))
    }
    const activities = body.activities.filter(activity => activity.startTime.indexOf(date) === 0)

    for (const activity of activities) {
      try {
        activity.source.trackerFeatures = activity.source.trackerFeatures.sort()
      } catch (err) {
        console.warn(err)
      }
    }

    callback(null, activities)
  })
}

const readdirAsync = promisify(readdir)
const readFileAsync = promisify(readFile)

function getFile (filename) {
  return readFileAsync(filename, 'utf8')
}

tools.getGarminActivities = async (date, callback) => {
  date = tools.isoDate(date)
  const { runFile } = getConfig()
  const dataFolder = dirname(runFile)
  const regex = new RegExp('^' + date + '.*summary.json')
  readdirAsync(dataFolder).then(files => {
    files = files.filter(file => regex.test(file))
      .map(file => resolve(dataFolder, file))
    return Promise.all(files.map(getFile))
  }).then(function (files) {
    const summary = files.map(file => {
      const json = JSON.parse(file)
      const logId = json.activityUUID.uuid
      const startTime = json.summaryDTO.startTimeLocal
      const duration = json.summaryDTO.movingDuration * 1000
      const distance = json.summaryDTO.distance / 1000
      const distanceUnit = 'KM'
      return { ...json.summaryDTO, ...json, distance, distanceUnit, duration, startTime, logId }
    })
    // console.log({ summary })
    callback(null, summary)
  })
}

// tools.getActivities = tools.getFitbitActivities.bind(tools)
// tools.getActivities = tools.getGarminActivities.bind(tools)

const noNulls = obj => {
  Object.keys(obj).forEach(key => {
    if (obj[key] === null) delete obj[key]
  })
  return obj
}

tools.getEverything = async (date, callback) => {
  if (process.env.CACHE) return callback(null, getOptionalJSONFile('/tmp/activities'))
  await GCClient.login(process.env.GUSER, process.env.GPASS)
  const userInfo = await GCClient.getUserInfo()
  const displayName = userInfo && userInfo.displayName // || 'd99659ec-272c-4f3f-8f1d-7cd8753150f7'
  if (!displayName) {
    console.info({ userInfo })
    // return callback(new Error('failed to login to garmin'))
    return callback(null, [], {})
  }

  const stepData = await GCClient.getSteps(new Date(date))
  const steps = stepData.reduce((steps, activity) => {
    return steps + activity.steps
  }, 0)

  const weightURL = 'https://connect.garmin.com/modern/proxy/weight-service/weight/latest'
  const weight = noNulls(await GCClient.get(weightURL, { date }) || {})
  if (weight.weight) weight.weight = weight.weight / 1000
  if (weight.bodyFat) weight.fat = weight.bodyFat

  const summaryURL = 'https://connect.garmin.com/modern/proxy/usersummary-service/usersummary/list/'
  const originalSummary = await GCClient.get(summaryURL + displayName, { start: 1, limit: 1, maxDate: date })
  const summary = noNulls(originalSummary.find(line => line.calendarDate === date) || {})

  const originalActivities = await GCClient.getActivities(0, 5)
  const activities = originalActivities.filter(activity => {
    return activity.startTimeLocal.substr(0, 10) === date
  }).map(activity => {
    /* { activity:
   { activityId: 6600968677,
     activityName: 'Shepway Walking',
     startTimeLocal: '2021-04-13 18:00:58',
     startTimeGMT: '2021-04-13 17:00:58',
     activityType:
      { typeId: 9,
        typeKey: 'walking',
        parentTypeId: 17,
        sortOrder: 27,
        isHidden: false,
        restricted: false },
     eventType: { typeId: 9, typeKey: 'uncategorized', sortOrder: 10 },
     distance: 4927.1201171875,
     duration: 3217.833984375,
     elapsedDuration: 3217833.984375,
     movingDuration: 2970.2459716796875,
     elevationGain: 27.17000000178814,
     elevationLoss: 24.029999999329448,
     averageSpeed: 1.531000018119812,
     maxSpeed: 1.968999981880188,
     startLatitude: 51.07291046530008,
     startLongitude: 1.1236351914703846,
     hasPolyline: true,
     ownerId: 79852077,
     ownerDisplayName: 'd99659ec-272c-4f3f-8f1d-7cd8753150f7',
     ownerFullName: 'Paul Clarke',
     ownerProfileImageUrlSmall:
      'https://s3.amazonaws.com/garmin-connect-prod/profile_images/fd0f3ba9-b795-4069-96aa-9ea503c03369-79852077.png',
     ownerProfileImageUrlMedium:
      'https://s3.amazonaws.com/garmin-connect-prod/profile_images/af57e42a-e2f4-436b-9d11-2b3f732ef91c-79852077.png',
     ownerProfileImageUrlLarge:
      'https://s3.amazonaws.com/garmin-connect-prod/profile_images/ca30d4d9-8643-4919-a1bc-d87932846fc4-79852077.png',
     calories: 389,
     averageHR: 115,
     maxHR: 153,
     averageRunningCadenceInStepsPerMinute: 111.421875,
     maxRunningCadenceInStepsPerMinute: 139,
     steps: 6080,
     userRoles:
      [ 'ROLE_OUTDOOR_USER',
        'ROLE_CONNECTUSER',
        'ROLE_FITNESS_USER',
        'ROLE_WELLNESS_USER' ],
     privacy: { typeId: 2, typeKey: 'private' },
     userPro: false,
     hasVideo: false,
     timeZoneId: 159,
     beginTimestamp: 1618333258000,
     sportTypeId: 11,
     avgPower: null,
     maxPower: null,
     aerobicTrainingEffect: 2.0999999046325684,
     anaerobicTrainingEffect: 0,
     strokes: null,
     normPower: null,
     leftBalance: null,
     rightBalance: null,
     avgLeftBalance: null,
     max20MinPower: null,
     avgVerticalOscillation: null,
     avgGroundContactTime: null,
     avgStrideLength: 82.61850956439322,
     avgFractionalCadence: null,
     maxFractionalCadence: null,
     trainingStressScore: null,
     intensityFactor: null,
     vO2MaxValue: 52,
     avgVerticalRatio: null,
     avgGroundContactBalance: null,
     lactateThresholdBpm: null,
     lactateThresholdSpeed: null,
     maxFtp: null,
     avgStrokeDistance: null,
     avgStrokeCadence: null,
     maxStrokeCadence: null,
     workoutId: null,
     avgStrokes: null,
     minStrokes: null,
     deviceId: 3318415640,
     minTemperature: 17,
     maxTemperature: null,
     minElevation: 11600,
     maxElevation: 13480.000305175781,
     avgDoubleCadence: null,
     maxDoubleCadence: 139,
     summarizedExerciseSets: null,
     maxDepth: null,
     avgDepth: null,
     surfaceInterval: null,
     startN2: null,
     endN2: null,
     startCns: null,
     endCns: null,
     summarizedDiveInfo:
      { weight: null,
        weightUnit: null,
        visibility: null,
        visibilityUnit: null,
        surfaceCondition: null,
        current: null,
        waterType: null,
        waterDensity: null,
        summarizedDiveGases: [],
        totalSurfaceTime: null },
     activityLikeAuthors: null,
     avgVerticalSpeed: null,
     maxVerticalSpeed: 0.5,
     floorsClimbed: null,
     floorsDescended: null,
     manufacturer: 'GARMIN',
     diveNumber: null,
     locationName: 'Shepway',
     bottomTime: null,
     lapCount: 5,
     endLatitude: 51.07215693220496,
     endLongitude: 1.1098050512373447,
     minAirSpeed: null,
     maxAirSpeed: null,
     avgAirSpeed: null,
     avgWindYawAngle: null,
     minCda: null,
     maxCda: null,
     avgCda: null,
     avgWattsPerCda: null,
     flow: null,
     grit: null,
     jumpCount: null,
     caloriesEstimated: null,
     caloriesConsumed: null,
     waterEstimated: 483,
     waterConsumed: null,
     maxAvgPower_1: null,
     maxAvgPower_2: null,
     maxAvgPower_5: null,
     maxAvgPower_10: null,
     maxAvgPower_20: null,
     maxAvgPower_30: null,
     maxAvgPower_60: null,
     maxAvgPower_120: null,
     maxAvgPower_300: null,
     maxAvgPower_600: null,
     maxAvgPower_1200: null,
     maxAvgPower_1800: null,
     maxAvgPower_3600: null,
     maxAvgPower_7200: null,
     maxAvgPower_18000: null,
     excludeFromPowerCurveReports: null,
     totalSets: null,
     activeSets: null,
     totalReps: null,
     minRespirationRate: null,
     maxRespirationRate: null,
     avgRespirationRate: null,
     trainingEffectLabel: 'AEROBIC_BASE',
     activityTrainingLoad: 34.35430908203125,
     avgFlow: null,
     avgGrit: null,
     minActivityLapDuration: 583.6489868164062,
     avgStress: null,
     startStress: null,
     endStress: null,
     differenceStress: null,
     maxStress: null,
     aerobicTrainingEffectMessage: 'MAINTAINING_AEROBIC_BASE_7',
     anaerobicTrainingEffectMessage: 'NO_ANAEROBIC_BENEFIT_0',
     splitSummaries: [],
     hasSplits: false,
     hasSeedFirstbeatProfile: null,
     favorite: false,
     decoDive: null,
     purposeful: false,
     manualActivity: false,
     elevationCorrected: false,
     atpActivity: false,
     pr: false,
     autoCalcCalories: false,
     parent: false } } */
    const logId = activity.activityID
    const startTime = activity.startTimeLocal
    const duration = activity.movingDuration * 1000
    const distance = activity.distance / 1000
    const distanceUnit = 'KM' // @todo get this from userInfo
    return noNulls({ ...activity, distance, distanceUnit, duration, startTime, logId })
  })

  console.log({ displayName, date, weight, summary, steps })
  callback(null, activities, noNulls({ ...weight, ...summary, steps }))
}

const isARun = activity => {
  if (!/(run|Treadmill)/i.test(activity.activityName)) return false
  if (activity.duration < 600000) return false
  if (actualDistance(activity) < 1) return false
  return true
}

tools.getLastRun = (date, callback) => {
  tools.getRuns(date, (err, runs) => {
    if (err) return callback(err)
    callback(null, runs.pop())
  })
}

tools.getRuns = (date, callback) => {
  tools.getActivities(date, (err, activities = []) => {
    if (err) return callback(err)
    callback(null, activities.filter(isARun))
  })
}

tools.getSlackToken = (host, code, callback) => {
  const url = 'https://slack.com/api/oauth.access'
  const redirect_uri = `http://${host}`
  const client_id = process.env.SLACK_CLIENT_ID || slack.client_id
  const client_secret = process.env.SLACK_CLIENT_SECRET || slack.client_secret
  const auth = btoa(`${client_id}:${client_secret}`)
  const headers = {
    'Authorization': `Basic ${auth}`
  }
  const qs = { redirect_uri, code }
  request.post({ url, headers, qs, json }, (err, response, body) => {
    // console.log({ client_id, client_secret, url, qs, headers, err, body })
    if (!err) err = body && body.error
    callback(err, body)
  })
}
