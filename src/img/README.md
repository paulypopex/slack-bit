# slack-bit

## Update slack with fitbit run stats

It will prompt you to set a slack token if you do not already have one, and a fitbit token, and will generate a default config file that you can edit. If you have an image saved as `src/img/profile.jpg` (or anything starting `profile` and ending `.jpg`) it will post a version of this as your profile, enhanced with your run stats. If the name of the image matches a number, an x, a number, it will use those as the coordinates to write in the picture. So for example
- src/img/profile.20x20.jpg adds your stats at 20% of the picture across, 20% of the picture down
- src/img/profile1.10x50.jpg adds your stats at 10% of the picture across, 50% of the picture down
- src/img/profile-foo.50x90.jpg adds your stats at 50% of the picture across, 90% of the picture down
