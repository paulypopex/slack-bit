#!/usr/bin/env node

// update slack status based on wifi access point

const { platform } = require('os')
const { execSync } = require('child_process')
const { updateStatus, configFile, getConfig, setConfig } = require('./tools')
const { updateCron } = require('./lib')
const config = getConfig()

// lifted untouched from https://github.com/LukaszWiktor/slack-status-based-on-wifi-name
const getLinuxWiFiName = () => execSync('iwgetid -r') // Linux only
  .toString()
  .split('\n')
  .filter(line => line.match(/.+/))
  .find(ssid => true) // find first

// lifted untouched from https://github.com/LukaszWiktor/slack-status-based-on-wifi-name
const getWinWiFiName = () => execSync('netsh wlan show interfaces') // Windows only
  .toString()
  .split('\n')
  .filter(line => line.includes(' SSID '))
  .map(line => line.match(/: (.*)/)[1])
  .find(ssid => true) // find first

const getMacWiFiName = () => {
  const info = '' + execSync('/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I')
  const ssid = info.match(/\bSSID: (.*)/)[1]
  if (!ssid) return null
  const addr = info.match(/\bBSSID: (.*)/)[1]
  return { ssid, addr }
}

const getWifi = platform => {
  switch (platform) {
    case 'darwin': return getMacWiFiName()
    case 'win32': return getWinWiFiName() // untested
    case 'linux': return getLinuxWiFiName() // untested
  }
  throw new Error(`Unknown platform ${platform}`)
}

let ssid = getWifi(platform())
if (!ssid) throw new Error('no ssid; are you on wifi?')
const addr = ssid.addr
ssid = ssid.ssid || ssid // (did we return an object?)
const key = addr || ssid
if (!config.statusBySSID || !config.statusBySSID[key]) {
  config.statusBySSID = config.statusBySSID || {}
  const _explanation = `Set automatically when connected to ${ssid} - ok to delete this line`
  const status = `Looks like I moved to a new wifi hotspot in ${ssid}`
  const emoji = ':shrug:'
  config.statusBySSID[ssid] = { _explanation, status, emoji }
  console.warn(`👍 updated config with ${JSON.stringify(config.statusBySSID)}`)
  console.warn(`Now edit ${configFile} to confirm, and run this command again`)
  if (addr && !config.statusBySSID[addr]) {
    console.warn(`⚠️ set an entry for this network ${ssid} and this access point ${addr} - feel free to delete either`)
    config.statusBySSID[addr] = { _explanation, status, emoji }
  }
  setConfig(config, err => {
    if (err) console.error(err)
    process.exit(err ? 1 : 0)
  })
} else {
  const { status, emoji } = config.statusBySSID[addr || ssid]
  updateStatus(status, emoji, err => {
    if (err) {
      if (err === 'invalid_auth') {
        const url = 'https://api.slack.com/custom-integrations/legacy-tokens'
        console.error(`🙀 your slack token "${process.env.SLACK_TOKEN}" is invalid - generate a new one at ${url} and save it.`)
        console.error(`EXPORT it in your ~/.bash_profile (see a friendly engineer) or set it when you run this script`)
        console.error(`SLACK_TOKEN=your-new-token ${process.argv.join(' ')}`)
        return
      }
      return console.error(err)
    }
  })
  updateCron(process.argv[1], '*/10 8-17 * * 1-5', err => {
    if (err) console.error(err)
  })
}
