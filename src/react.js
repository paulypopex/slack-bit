#!/usr/bin/env node

const { search, react } = require('./tools')

search(process.argv.slice(2).join(' '), (err, messages) => {
  if (err) throw err
  messages.forEach(message => {
    console.log(message)
    const name = 'thumbsup'
    const timestamp = message.ts
    const channel = message.channel.id
    react(channel, timestamp, name)
  })
})
