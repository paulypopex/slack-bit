#!/usr/bin/env node

// read my run stats from fitbit and prepare a blog post

const { readFile, writeFile } = require('fs')
const { dirname, join } = require('path')
const { distance, duration, getEverything, getOptionalJSONFile, nonEmpty, time, getConfig, pace, reorder, setJSONFile, isoDate } = require('./tools')
const { blogFolder = process.env.HOME, fatFile, runFile, stepFile, treadmillCost, treadmillFile } = getConfig()
const mkdirp = require('mkdirp')
const svg2img = require('svg2img')
let dateModifier = process.argv[2]
let date = new Date()
const minusDays = /^-(\d+)$/.exec(dateModifier)
if (minusDays) {
  date.setDate(date.getDate() - minusDays[1])
} else if (dateModifier) {
  date = new Date(dateModifier)
}
date.setHours(23)
date.setMinutes(59, 59, 999)
console.log({ date })
const itsame = date.getDay() === 6
const dateStamp = isoDate(date)
const today = isoDate()
const treadmillData = getOptionalJSONFile(treadmillFile)
const runData = getOptionalJSONFile(runFile)
const fatData = getOptionalJSONFile(fatFile)
const stepData = getOptionalJSONFile(stepFile)

const getDistancePerStep = (best, run) => {
  if (!run.distance) return best
  if (!run.steps) return best
  best.distance = (best.distance || 0) + run.distance
  best.steps = (best.steps || 0) + run.steps
  best.distancePerStep = best.distance / best.steps
  return best
}

const distancePerStep = Object.values(runData).reduce(getDistancePerStep, {}).distancePerStep

const days = 10
const lastPeriod = new Date(date)
lastPeriod.setDate(lastPeriod.getDate() - days)

const getDay = (date, short) => {
  date = new Date(date)
  const month = 'short'
  const weekday = 'short'
  const day = 'numeric'
  const year = '2-digit'
  let options = { weekday, day, month, year }
  if ((new Date(today).getTime() - date.getTime()) < 7 * 24 * 60 * 60 * 1000) {
    options = { weekday: 'long' }
  } else if (short) {
    options = { weekday }
  } else if ((new Date(today).getYear() === date.getYear())) {
    options = { weekday, day, month }
  }
  try {
    return new Intl.DateTimeFormat('en-US', options).format(date)
  } catch (e) {
    console.error({ date, options }, e)
    return date
  }
}

const getMonths = date => {
  return date.getFullYear() * 12 + date.getMonth()
}

// not really pixels but SVG viewbox units
const stepsToPixels = steps => Math.floor(steps / 10)

const isThisPeriod = dateForComparison => {
  if (!isBefore(dateForComparison)(lastPeriod)) {
    return false
  }
  if (isBefore(dateForComparison)(dateStamp)) {
    return false
  }
  return true
}

const isBefore = comparison => date => {
  return isoDate(date) < isoDate(comparison)
}

const beforeThisPeriod = date => {
  return isoDate(date) <= isoDate(lastPeriod)
}

const infographic = stepData => {
  const margin = 200
  const fontSize = margin * 0.9
  const marginTop = margin * 2
  const marginBottom = margin * 2
  const marginLeft = margin * 2
  const firstBetterThan = bestDayThisPeriod => (previousBest, key) => {
    if (previousBest) return previousBest
    if (!bestDayThisPeriod) return key
    if (!stepData[bestDayThisPeriod]) return key
    if (stepData[key].steps > stepData[bestDayThisPeriod].steps) {
      return key
    }
  }
  const worstDayBySteps = (worst, key) => {
    if (key === today) return worst // today is not yet complete
    if (!stepData[worst]) return key
    if (!stepData[worst].steps) return key
    if (stepData[key].steps < stepData[worst].steps) return key
    return worst
  }
  const bestDayBySteps = (best, key) => {
    if (!stepData[best]) return key
    if (!stepData[best].steps) return key
    if (stepData[key].steps > stepData[best].steps) return key
    return best
  }

  const bestDay = Object.keys(stepData).reduce(bestDayBySteps)
  const bestSteps = stepData[bestDay].steps
  const daysThisPeriod = Object.keys(stepData).filter(isThisPeriod).sort()
  const bestDayThisPeriod = daysThisPeriod.reduce(bestDayBySteps, {})
  const worstDayThisPeriod = daysThisPeriod.reduce(worstDayBySteps, {})
  const worstStepsThisPeriod = (stepData[worstDayThisPeriod] || {}).steps || 0
  const previousBestDay = Object.keys(stepData).filter(beforeThisPeriod).sort().reverse().reduce(firstBetterThan(bestDayThisPeriod), null)
  const previousBestSteps = previousBestDay ? stepData[previousBestDay].steps : 0
  const bestStepsThisPeriod = (stepData[bestDayThisPeriod] || {}).steps || 0
  // const bestStepsThisPeriod = Math.max.apply(null, Object.keys(stepData).filter(isThisPeriod).map(key => stepData[key].steps || 0))
  const maxHeight = stepsToPixels(bestSteps)
  const maxWidth = maxHeight * 5 // to get image proportions // @todo get from days
  const imageHeight = maxHeight + marginTop + marginBottom
  const imageWidth = maxWidth + marginLeft + margin
  const barWidth = Math.round(imageWidth / ((days + 1) * 2))
  const strokeWidth = Math.floor(barWidth / 30)
  const border = '#000'

  const stepBars = daysThisPeriod.map((key, index) => {
    let x = index * 2 * barWidth + barWidth + margin
    if (!itsame) x = x - barWidth + margin
    const width = itsame ? barWidth : barWidth * 2
    const steps = stepData[key].steps
    const height = stepsToPixels(steps)
    const y = maxHeight + marginTop - height
    const stepsLabel = key === today ? `${steps} (so far)` : steps
    let star = `<text x="${width / 2}" y="${y - 20}" fill="${border}" dominant-baseline="top">${stepsLabel}</text>`
    if (itsame) {
      if (key === isoDate(date)) {
        star = `<use href="/img/sprite.svg#itsame" width="${barWidth * 1.5}" height="${barWidth * 1.5}" x="${barWidth * -0.4}" y="${Math.round(y - barWidth * 2)}" />` + star
      } else if (steps <= worstStepsThisPeriod) {
        const cloudY = marginTop // Math.floor(y - barWidth * 2)
        star = `<use href="/img/sprite.svg#cloud" stroke="grey" width="${barWidth * 2}" height="${barWidth * 2}" x="${barWidth * -0.5}" y="${cloudY}"/>
      <g transform="translate(${width / 2}, ${cloudY + barWidth})" fill="${border}">
        <text>${Object.keys(stepData).length} days</text>
        <text y="${Math.round(barWidth * 0.2)}">of data</text>
      </g>` + star
      } else if (steps >= bestStepsThisPeriod) {
        star = `<use href="/img/sprite.svg#star" fill="yellow" stroke="red" stroke-width="1" width="${barWidth}" height="${barWidth}" y="${y - barWidth * 1.5}" />
      <text x="${width / 2}" y="${y - barWidth}" fill="${border}">${stepsLabel}</text>`
      }
    }
    let bar = `<rect y="${y}" width="${barWidth * 2}" height="${height}" fill="lightgrey" />`
    if (itsame) {
      bar = `<use href="/img/sprite.svg#capped-pipe" y="${y}" height="${height}" width="${barWidth}" />`
    }
    return `<g transform="translate(${x}, 0)">
      ${star}
      ${bar}
      <text fill="${border}" x="${width / 2}" y="${y + height + margin}">${getDay(key, days <= 7)}</text>
    </g>`
  })

  const includingToday = includeToday => key => {
    if (key !== today) return true
    return Boolean(includeToday)
  }

  const line = (attribute, color, label, includeToday = false, min = null, max = null) => {
    let x1 = marginLeft
    let y1 = null
    let value = null
    const gotAttribute = key => stepData[key][attribute]
    const previousDayWithAttribute = Object.keys(stepData).filter(beforeThisPeriod).sort().filter(gotAttribute).pop()
    if (previousDayWithAttribute) value = stepData[previousDayWithAttribute][attribute]
    if (min === null) {
      min = Math.min.apply(null, Object.keys(stepData)/* .filter(isThisPeriod) */.filter(gotAttribute).map(key => stepData[key][attribute]))
    }
    if (max === null) {
      max = Math.max.apply(null, Object.keys(stepData)/* .filter(isThisPeriod) */.filter(gotAttribute).map(key => stepData[key][attribute]))
    }
    const range = max - min
    if (value !== null) {
      const initialValueAsSteps = ((value - min) / range) * bestSteps
      y1 = maxHeight + marginTop - stepsToPixels(initialValueAsSteps)
    }

    let x, y
    const points = daysThisPeriod.filter(gotAttribute).filter(includingToday(includeToday)).map((key, index) => {
      x = index * 2 * barWidth + barWidth + margin
      value = stepData[key][attribute]
      const valueAsSteps = ((value - min) / range) * bestSteps
      const height = stepsToPixels(valueAsSteps)
      y = maxHeight + marginTop - height
      return [x, y]
    }).filter(Boolean)
    points.unshift([x1, y1])
    return `<g stroke="${color}">
      <polyline points="${points.join(' ')}" fill="none" />
    </g>
    <text fill="${color}" x="${x + barWidth * 1.2}" y="${y}">${Number(value).toFixed(1).replace('.0', '')}${label}</text>`
  }

  const lines = [
    // line('sedentaryMinutes', 'brown', ' minutes at rest'),
    // line('veryActiveMinutes', 'green', ' very active minutes', false, 0, 120),
    line('restingHeartRate', 'pink', ' bpm ❤︎', true, 32), // include today on this line
    line('averageStressLevel', 'blue', '% stress', true), // include today on this line + force range
    // line('fat', 'red', '% fat', true, 9, 30), // include today on this line + force range
    // line('weight', 'blue', 'kg', true, 60, 70), // include today on this line + force range
    line('dailyStepGoal', 'green', ' my target', true, 0, bestSteps) // include today on this line + force range
  ]
  let axis = `<path d="m ${marginLeft} ${marginTop} v ${maxHeight} h ${maxWidth} h -${maxWidth} v -${maxHeight} z" stroke="${border}" stroke-width="${strokeWidth}" />\n`
  const interval = 10000
  axis += `    <g fill="${border}" transform="translate(${marginLeft}, ${maxHeight + marginTop})">\n`
  for (let i = interval; i < bestSteps; i += interval) {
    axis += `      <text y="-${stepsToPixels(i)}">${Math.round(i / 1000)}k -</text>\n`
  }
  axis += '    </g>'

  const tenKY = maxHeight + marginTop - stepsToPixels(10000)
  const tenKLine = `<text fill="lightgrey" x="${maxWidth + marginLeft}" y="${tenKY}">your daily target</text>
    <line x1="${marginLeft}" y1="${tenKY}" x2="${maxWidth - barWidth}" y2="${tenKY}" stroke="lightgrey" stroke-width="${Math.round(strokeWidth / 2)}" stroke-dasharray="${margin}" />`

  const bestDayMessage = previousBestDay ? `<text y="${margin}">${getDay(bestDayThisPeriod, 'short')} was the best since ${getDay(previousBestDay)}'s ${previousBestSteps}</text>` : ''
  const fill = itsame ? 'url(#background)' : 'transparent'
  return `
<svg viewBox="0 0 ${imageWidth} ${imageHeight}" fill="${fill}" font-size="${fontSize}" xmlns="http://www.w3.org/2000/svg">
  <rect x="${marginLeft}" y="${marginTop}" width="${maxWidth}" height="${maxHeight}" />
  <g text-anchor="end">
    ${tenKLine}
    <g fill="${border}" transform="translate(${maxWidth + marginLeft}, ${marginTop})">
      <text>Best ever daily steps: ${bestSteps} on ${getDay(bestDay)}</text>
      ${bestDayMessage}
    </g>
    ${axis}
  </g>
  <g stroke-width="${Math.round(strokeWidth / 2)}">
    ${lines.join('\n    ')}
  </g>
  <g text-anchor="middle" opacity="0.9" stroke="transparent">
    ${stepBars.join('\n    ')}
  </g>
</svg>`
}

const replaceFile = exports.replaceFile = (fullPath, content = '', callback) => {
  readFile(fullPath, 'utf8', (ignoredError, original = '') => {
    if (original === content) return callback() // nothing changed
    console.log('🆕', fullPath)
    if (/^---/.test(content)) {
      // it's got frontmatter
      const originalWithoutFrontMatter = original.replace(/[\s\S]+?^---$/m, '')
      const contentWithoutFrontMatter = content.replace(/[\s\S]+?^---$/m, '')
      if (originalWithoutFrontMatter !== contentWithoutFrontMatter) {
        content = content.replace(/lastmod: .+\n/, '')
        const lastmod = new Date().toISOString().substr(0, 10)
        const match = /^date: (.+)$/m.exec(content)
        if (match) {
          const date = new Date(match[1]).toISOString().substr(0, 10)
          if (date !== lastmod) {
            content = content.replace(/^---$/m, `---\nlastmod: ${lastmod}`)
          }
        }
      }
    }
    mkdirp(dirname(fullPath)).then(() => {
      writeFile(fullPath, content, 'utf8', callback)
    })
  })
}

const svg = infographic(stepData)
svg2img(svg, { width: 600, height: 315 }, (err, contents) => {
  if (err) console.error(err)
  writeFile(`${process.env.HOME}/Sites/clarkeology.com/_site/infographic.png`, contents, err => {
    if (err) console.error(err)
  })
})

const getCachedData = (data, dateStamp) => Object.keys(data).reduce((all, id) => {
  if (!data[id].startTime.includes(dateStamp)) return all
  all[id] = data[id]
  return all
}, {})

writeFile(`${process.env.HOME}/Sites/clarkeology.com/_site/infographic.html`, svg, err => {
  if (err) throw err
  if (process.env.DEBUG) {
    process.exit(1)
  }

  getEverything(dateStamp, (err, activities = [], summary = {}) => {
    if (err) throw err
    if (!process.env.CACHE) {
      setJSONFile('/tmp/activities.json', activities, err => {
        if (err) console.error(err)
      })
    }
    // console.log('now do we have stepData for', { dateStamp }, stepData[dateStamp])
    summary = { ...stepData[dateStamp], ...summary }
    console.log('had ids', activities.map(activity => activity.logId || activity.activityId))
    const cachedRunData = getCachedData(runData, dateStamp)
    for (const id in cachedRunData) {
      if (!activities.map(activity => String(activity.logId || activity.activityId)).includes(id)) {
        console.log('did not have run ', id, 'in', activities.map(activity => activity.logId || activity.activityId))
        activities.push(cachedRunData[id])
      }
    }
    const cachedTreadmillData = getCachedData(treadmillData, dateStamp)
    for (const id in cachedTreadmillData) {
      if (!activities.map(activity => String(activity.logId || activity.activityId)).includes(id)) {
        activities.push(cachedTreadmillData[id])
      }
    }
    console.log('now got', activities.map(activity => activity.logId || activity.activityId))
    // if (dateStamp === today) buildProfileImage(activities[0], () => {})
    let tags = {}
    let title = null
    let subTitle = ''
    let content = activities.map(activity => {
      activity = reorder(activity)
      if (/cardio/i.test(activity.activityName)) {
        console.warn(activity)
        title = title || 'Exercise stats'
        tags.exercise = true
        return `- 🏋️ ${time(activity)} ${activity.activityName} ${duration(activity.summaryDTO.duration)}`
      }
      if (/walk/i.test(activity.activityName)) {
        title = title || 'Exercise stats'
        tags.exercise = true
        return `- 🚶 ${time(activity)} ${activity.activityName} ${duration(activity.duration / 1000)}`
      }
      if (/(bike|cycl)/i.test(activity.activityName)) {
        title = title || 'Exercise stats'
        tags.exercise = true
        tags.bike = true
        return `- 🚴 ${time(activity)} ${activity.activityName} ${distance(activity, distancePerStep)}`
      }
      if (/treadmill/i.test(activity.activityName)) {
        title = title || 'Run stats'
        tags.exercise = true
        tags.treadmill = true
        treadmillData['' + (activity.logId || activity.activityId)] = activity
        const parsedDistance = parseInt(distance(activity, distancePerStep), 10)
        if (parsedDistance > 15) tags['hey hey 16k'] = true
        else if (parsedDistance > 9) tags['10k'] = true
        else if (parsedDistance > 4) tags['5k'] = true
        if (subTitle === '') subTitle = ` - ${pace(activity, distancePerStep)}`
        return `- 🐹 ${time(activity)} ${activity.activityName} ${duration(activity.duration / 1000)} ${distance(activity, distancePerStep)}, ${pace(activity, distancePerStep)}`
      }
      if (/run/i.test(activity.activityName)) {
        title = 'Run stats'
        tags.exercise = true
        tags.running = true
        runData['' + (activity.logId || activity.activityId)] = activity
        const parsedDistance = parseInt(distance(activity, distancePerStep), 10)
        if (parsedDistance > 15) tags['hey hey 16k'] = true
        else if (parsedDistance > 9) tags['10k'] = true
        else if (parsedDistance > 4) tags['5k'] = true
        subTitle = ` - ${pace(activity, distancePerStep)}`
        return `- 🏃 ${time(activity)} ${activity.activityName} ${distance(activity, distancePerStep)}, ${pace(activity, distancePerStep)}`
      }
      console.warn('?', activity.activityName, activity) // not one of my events, unlikely
    }).filter(nonEmpty)
    if (content.length) content.unshift(`\n`)

    if (summary.floors) {
      content.unshift(`🧗 ${summary.floors} floors`)
    }
    if (summary.weight > 60) {
      tags.fat = true
      fatData[dateStamp] = reorder({ ...summary, ...fatData[dateStamp] })
      if (summary.bodyFat) content.unshift(`${Number(summary.bodyFat).toFixed(1)}% fat`)
      content.unshift(`${Number(summary.weight).toFixed(1)}kg`)
    }
    if (summary.restingHeartRate) {
      content.unshift(`❤️  ${summary.restingHeartRate} bpm`)
    }
    if (summary.steps) {
      tags['step count'] = true
      content.unshift(`🚶 ${summary.steps} steps`)
    }

    if (tags.treadmill && treadmillCost && treadmillFile) {
      const keys = Object.keys(treadmillData).filter(key => {
        return new Date(treadmillData[key].startTime) < date
      })

      const uses = keys.length
      const dates = keys.map(key => treadmillData[key].startTime).sort()

      const firstUse = new Date(dates.shift())
      const lastUse = new Date(dates.pop() || firstUse)
      const months = (getMonths(lastUse) - getMonths(firstUse)) + 1
      // console.log({ uses, months, lastMonths: getMonths(lastUse), firstMonths: getMonths(firstUse), lastUse, firstUse })

      content.push(`\n<seo>`)
      content.push(`\n${uses} treadmill uses at [£${(treadmillCost / uses).toFixed(2)} per go](/2019/01/28/i-will-work-out-what-the-treadmill-costs-per-go/), or £${(treadmillCost / months).toFixed(2)} per month.`)
      content.push(`Including all the times Clare has used it, this probably comes to about £${(treadmillCost / (uses + 10)).toFixed(2)} per go...`)
      const daysOfLockDown = Math.floor((new Date('2020-07-20').getTime() - new Date('2020-04-01').getTime()) / (24 * 60 * 60 * 1000))
      content.push(`Including all the times the boys have (they went on it every day during ${daysOfLockDown} days of the 2020 school lockdown), this comes to about £${(treadmillCost / (uses + daysOfLockDown)).toFixed(2)} per go...`)
      content.push(`\n</seo>`)
      setJSONFile(treadmillFile, treadmillData, err => {
        if (err) console.error(err)
      })
    }

    if (tags.fat && fatFile) {
      setJSONFile(fatFile, fatData, err => {
        if (err) console.error(err)
      })
    }

    if (tags.running && runFile) {
      setJSONFile(runFile, runData, err => {
        if (err) console.error(err)
      })
    }

    if (summary.steps && stepFile) {
      stepData[dateStamp] = reorder(summary)
      setJSONFile(stepFile, stepData, err => {
        if (err) console.error(err)
      })
      if (dateStamp > '2019-08-14') {
        if (itsame || (dateStamp < '2019-10-23')) { // only update on Saturdays for now
          const svg = infographic(stepData)
          content.push(svg.replace(/ {2,}/mg, '  ').replace(/\n\s*\n/mg, '\n').replace(/ *$/mg, ''))
        }
      }
    }

    if (!content.length) {
      console.log('no content')
      process.exit(1)
    }
    const year = String(date.getFullYear())
    const file = join(blogFolder, year, `${dateStamp}-run-stats.md`)
    const pad = number => number < 10 ? '0' + number : number
    const post = `---
title: ${title || 'Health stats'} ${date.getDate()}/${date.getMonth() + 1}/${date.getYear() % 100}${subTitle}
date: ${dateStamp} 08:00:00
permalink: /${date.getFullYear()}/${pad(date.getMonth() + 1)}/${pad(date.getDate())}/run-stats/
board: 3
priority: 0.2
automated: true
layout: post.html
tags: [${Object.keys(tags).join(', ')}]
---
${content.join('\n')}`
    replaceFile(file, post, err => {
      if (err) throw err
    })
  })
})
