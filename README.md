# slack-bit

Slack + fitbit utils

To install from this repo:
```
git clone https://gitlab.com/paulypopex/slack-bit.git
cd slack-bit
npm install
```
This gives you the `npm run` scripts as below.

## Authorise

You need a slack client id, so pass this in first time you use this, so:
```
SLACK_CLIENT_ID=[your id here] SLACK_CLIENT_SECRET=[your secret here] npm run slack-auth
```
and if you're using the Fitbit parts:
```
FITBIT_CLIENT_ID=[your id here] FITBIT_CLIENT_SECRET=[your secret here] npm run auth-bit
```

you don't need the fitbit auth if you only want to use the wifi part of this...

## Update slack status based on wifi hotspot

```
git clone https://gitlab.com/paulypopex/slack-bit.git
cd slack-bit
npm install
npm run slack-wifi
```

It will prompt you to set a slack token if you do not already have one, and will generate a default config file that you can edit.

Inspired by https://github.com/LukaszWiktor/slack-status-based-on-wifi-name

### @todo

- move config to yml for easier editing?
- test it on other os, is only tested on mac so far

## Update slack with fitbit run stats

```
npm run slack-bit
```

[This could also update your profile image](./src/img/)...

## Blog fitbit run stats

Hmm less broad appeal here... prepare a blog post with today's run stats
```
npm run blog-bit
```
It will prepare a metalsmith blog post like what I use, in the folder defined as `tempFolder` in your config file.

## Install from npm

```
npm install slack-bit
```
This should put `slack-auth`, `slack-bit`, `slack-wifi` executables in your path, so no need for the `npm run` in front of all the commands above.

